//
//  FoodDrawerCollectionViewController.swift
//  FoodDrawer MessagesExtension
//
//  Created by Ievgen Gavrysh on 2/27/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

private let headerIdentifier = "Header"

class FoodDrawerCollectionViewController: UICollectionViewController {
    
    let numberOfItemsPerRow = 3.0 as CGFloat
    let interItemSpacing = 1.0 as CGFloat
    let interRowSpacing = 1.0 as CGFloat
    let sectionTitleKey = "SectionTitle"
    let sectionItemsKey = "Items"
    var data = [[String: AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.map {
            $0.delegate = self
            $0.dataSource = self
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.collectionView!.register(FoodDrawerSectionHeaderView.self,
                                      forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                      withReuseIdentifier: String(describing: FoodDrawerSectionHeaderView.self))

        self.collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        
        lift((
            UserDefaults.standard.array(forKey: FoodDrawerUserDefaults.SelectedSectionsKey) as? [Int],
            Bundle.main.path(forResource: FoodDrawerSettings.FileName,
                             ofType: FoodDrawerSettings.FileExtension)
                .flatMap { NSDictionary(contentsOfFile: $0) as? [String: AnyObject] }
                .flatMap { $0[FoodDrawerSettings.SectionsKey] as? [[String: AnyObject]] }
            ))
            .map { (tuple: ([Int], [[String: AnyObject]])) in
                tuple.0.reduce(into: [[String: AnyObject]](), { (result, index) in
                    result.append(tuple.1[index])
                })
            }
            .do {
                self.data = $0
            }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: -
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView,
                                 willDisplay cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath)
    {
        // Configure the cell
        guard let foodItem = cell as? FoodItemCell else {
            return
        }
        
        let sectionItems = self.data[indexPath.section][sectionItemsKey] as? [AnyObject]
        let imageName = sectionItems![indexPath.row] as! String
        foodItem.configure(usingImageName: imageName)
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    // MARK: -
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return data.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int
    {
        return (data[section][sectionItemsKey] as! NSArray).count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView
    {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                         withReuseIdentifier:String(describing: FoodDrawerSectionHeaderView.self),
                                                                         for: indexPath)
        if let foodHeader = headerView as? FoodDrawerSectionHeaderView {
            let section = self.data[indexPath.section]
            let sectionTitle = section[sectionTitleKey] as! String
            foodHeader.configure(usingTitle: sectionTitle)
        }
        
        return headerView
    }

}

//
//  File.swift
//  FoodDrawer MessagesExtension
//
//  Created by Ievgen Gavrysh on 2/27/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import UIKit

class FoodDrawerSectionHeaderView: UICollectionReusableView {
    
    @IBOutlet var sectionTitle: UILabel!
    
    func configure(usingTitle title:String) {
       // self.sectionTitle.text = title
    }
}

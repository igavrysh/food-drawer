//
//  FoodItemCell.swift
//  FoodDrawer MessagesExtension
//
//  Created by Ievgen Gavrysh on 2/27/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import Messages

class FoodItemCell: UICollectionViewCell {
    
    @IBOutlet weak var stickerView: MSStickerView!
    
    func configure(usingImageName imageName:String) {
        guard let imagePath = Bundle.main.path(forResource: imageName, ofType: ".png") else {
            return
        }
        
        let path =  URL(fileURLWithPath: imagePath)
        
        do {
            let description = NSLocalizedString("Food Sticker", comment: "")
            let sticker = try MSSticker(contentsOfFileURL: path , localizedDescription: description)
            stickerView.sticker = sticker
        } catch {
            fatalError("Failed to create sticker: \(error)")
        }
    }
}

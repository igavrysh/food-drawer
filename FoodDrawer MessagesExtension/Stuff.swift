//
//  Stuff.swift
//  FoodDrawer MessagesExtension
//
//  Created by Ievgen Gavrysh on 2/27/18.
//  Copyright © 2018 Ievgen Gavrysh. All rights reserved.
//

import Foundation

class FoodDrawerUserDefaults {
    static let SelectedSectionsKey = "selectedSections"
}


class FoodDrawerSettings {
    static let FileName = "FoodDrawerData"
    static let FileExtension = ".plist"
    static let SectionsKey = "Sections"
}
